(function (comp) {
    "use strict";
    var preloaderDOM = {
        dom: [{
            rect: ['730px', '357px', '294px', '283px', 'auto', 'auto'],
            id: 'persnoaje-4',
            fill: ['rgba(0,0,0,0)', 'images/persnoaje-4.png', '0px', '0px'],
            type: 'image',
            tag: 'img'
        }, {
            rect: ['387px', '205px', '250px', '229px', 'auto', 'auto'],
            id: 'Cargando',
            fill: ['rgba(0,0,0,0)', 'images/Cargando.gif', '0px', '0px'],
            type: 'image',
            tag: 'img'
        }],
        style: {
            '${symbolSelector}': {
                isStage: 'true',
                rect: [undefined, undefined, '1024px', '640px']
            }
        }
    }
    comp.definePreloader(preloaderDOM);
}(AdobeEdge.getComposition("EDGE-401161")));
(function (comp) {
    "use strict";
    var downLevelStageDOM = {
        dom: [],
        style: {
            '${symbolSelector}': {
                isStage: 'true',
                rect: [undefined, undefined, '1024px', '640px'],
                fill: ['rgba(255,255,255,1)']
            }
        }
    }
    comp.defineDownLevelStage(downLevelStageDOM);
}(AdobeEdge.getComposition("EDGE-401161")));