var navigation={
   length:12,
    masternav:1,
    /*intro*/
    nav1:1,/*3*/
    /*items_intro_camino*/
    nav2:1,/*10*/
    /*edificios*/
    nav3:1,/*4*/
    /**edificio 1*/
    nav4:1,
    /*items_edif1_nivel1*/
    nav5:1,/*4*/
    /*items_edif1_nivel2*/
    nav6:1,/*3*/
    /*items_edif1_nivel3*/
    nav7:1,/*3*/
    /*items_edif1_nivel4*/
    nav8:1,/*3*/
    nav9:1,/*7*/
    nav10:1,/*7*/
    nav11:1,/*7*/
    nav12:1/*7*/
}


var funtions_navigations={
    length:12,
    /*onNavintro*/
    onNav1: function (level) {
        nav1.ln=level; 
    },
    /*onNavitems_intro_camino*/
    onNav2:function(level){
        nav2.ln=level; 
        if(level==11){$(ST+"r1").css("visibility","hidden");}
    },
    /*onNavedificios*/
    onNav3:function(level){
        nav3.ln=level; 
    },
    /*onNavitems_edif1_nivel1*/
    onNav4:function(level){
        nav4.ln=level;
    },
    /*onNavitems_edif1_nivel2*/
    onNav5:function(level){
        nav5.ln=level
    },
    /*onNavitems_edif1_nivel3*/
    onNav6:function(level){
        nav6.ln=level
    },
    /*onNavitems_edif1_nivel4*/
    onNav7:function(level){
       nav7.ln=level
    },
    /*onNavitems_edif1_nivel5*/
    onNav8:function(level){
       nav8.ln=level
    },
    
    onNav9:function(level){
       nav9.ln=level
    },
    onNav10:function(level){
       nav10.ln=level
    },
    onNav11:function(level){
      nav11.ln=level
    },
    onNav12:function(level){
      nav12.ln=level
    }
}

class UdeC_SCORM {
constructor( debug) {
        this.debugger=debug;
        if(this.debugger==false){
            this.callSucceeded;//Specify SCORM 1.2:
            this.scorm = pipwerks.SCORM;
            this.scorm.version = "2004";
            console.log("Inicialización.");
            this.callSucceeded = this.scorm.init(); 
        }
    }
    complete() {
         if(this.debugger==false){
        this.callSucceeded = this.scorm.set("cmi.completion_status", "completed");
         }
    }
    end() {
         if(this.debugger==false){
        var callSucceeded = this.scorm.quit();
        return callSucceeded;
         }
    }
    get(item) {
        this.scorm.get(item);
    }
    set(item, save) {
        this.scorm.set(item, save);
    }
    //metodos mas utilizados//
    info_user() {
        var dates = {
            name: this.debugger==false ? String(this.scorm.get("cmi.learner_name")) : "Test User", 
            id: this.debugger==false ? String(this.scorm.get("cmi.learner_id")) : "Test identification"
        }
        return dates;
    }
    reset_commit(){
        this.set_commit("cmi.location",null);
    }
    set_commit(save) {
        if(this.debugger==false){
            save=JSON.stringify(save);
            this.scorm.set("cmi.location", save);//cmi.location guarda poca info//
            var callSucceeded = this.scorm.quit();
            this.complete();
            this.end();
        }else{
            save=JSON.stringify(save);
            localStorage.setItem("cmi.location", save);
        }
    };
    get_commit() {
        var commit = this.debugger==false ? String(this.scorm.get("cmi.location")) : String(localStorage.getItem("cmi.location"));
        if(commit=="" || commit==null ) { return null}else{return JSON.parse(commit);}
    };
    reset_commit(){
        alert("borrando");
        localStorage.setItem("cmi.location",null);
    }

    set_score(note) {
        if(this.debugger==false){
            this.set("cmi.score.raw", String(note));
            this.set("cmi.score.scaled", String(note / 100));
        }else{
            localStorage.setItem("cmi.score.raw", String(note));
            localStorage.setItem("cmi.score.scaled", String(note / 100));
        }
    };
    get_score() {
        notes = {
            raw: this.debugger==false ? this.scorm.get("cmi.score.raw") :  localStorage.getItem("cmi.score.raw"), 
            scaled: this.debugger==false ? this.scorm.get("cmi.score.scaled") : localStorage.getItem("cmi.score.scaled")
        }
        
    }

}
var reset=false;
var udec = new UdeC_SCORM(true);
class History_navigation{
    constructor(){
        this.debugger=true;
        this.read();
        this.save();
        this.testament();
    }
    read(){
        var commit = udec.get_commit();
        console.log(commit);
        if (commit != null) {
            navigation=commit; 
        }
    }
    save() {
        $("body").keypress(function (event) {
            if (event.which == 114) {
                reset=true;
                udec.reset_commit();
                location.reload();
                
            }
            if (event.which == 115) {
                udec.set_commit(navigation);
                console.log(udec.get_commit());
                location.reload();
                udec.complete();
                udec.end();
            }
        });
		
        $(window).unload(function () {
            if(reset==false){
                udec.set_commit(navigation);
                console.log(udec.get_commit());
                location.reload();
                udec.complete();
                udec.end();     
            }
           
        });
        window.onbeforeunload = function () {
            if(reset==false){
                
          
                udec.set_commit(navigation);
                console.log(udec.get_commit());
                location.reload();
                udec.complete();
                udec.end();     
            }
        };
    }
    set_value(id, value) {
        if (navigation[id] <= value) {
            navigation[id] = value;
            if (this.debugger) {
                console.log("navegador: " + id + " valor:" + value);
            }
        }
    }
    get_value(id){
        return navigation[id];
    }
    testament(){
        var limit=navigation.length;
     
        for(var i=1; i<=limit; i++){
            funtions_navigations["onNav"+i](navigation["nav"+i]);
        }
    }
}