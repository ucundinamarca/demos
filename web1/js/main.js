ST = "#Stage_";
S = "Stage_";
var depuracion=true;
var encendidoA1=true;
var json
var nav1,nav2,nav3,nav4,nav5,nav6,nav7,nav8,nav9,nav10,nav11,nav12;
var sonido1,sonido2,sonido3,sonido4,sonido5,sonido6,sonido7,sonido8,sonido9,sonido10,sonido11,sonido12,sonido13,sonido14;
var ciudadela,st1,st2,st3,st4,st5,st6,st61,st7,st8,st9,st10,credito,st11,st12,st11Comp1,zoom,edifx1,edifx2,edifx3,edifx4,historial;
edifx1={x:92,y:-27,element:ST+"bajaA"};
edifx2={x:48,y:-20,element:ST+"bajaA"};
edifx3={x:70,y:-20,element:ST+"bajaA"};
edifx4={x:0,y:-50,element:ST+"bajaA"};
ultimo="sonido4";
var indexaudio=0;
onplay=["none",false,false,false,false,false];
class UdeC {
    audio(onload){




        var folder="audios/";
        var audios=[
            {
                url:"Edifica_Resena.mp3",
                name:"sonido1"
            },{
                url:"Edificio_comentario_de_texto.mp3",
                name:"sonido2"
            },{
                url:"Edificio_Ensayo.mp3",
                name:"sonido3"
            },{
                url:"Edificio_estructura_de_los_textos_academicos.mp3",
                name:"sonido4"
            },{
                url:"Edificio_Trabajo_de_Investigacion.mp3",
                name:"sonido5"
            },{
                url:"Icono_hemeroteca_biblioteca.mp3",
                name:"sonido6"
            },{
                url:"Icono_Informacion_bibloteca.mp3",
                name:"sonido7"
            },{
                url:"Icono_Ingreso_Biblioteca.mp3",
                name:"sonido8"
            },{
                url:"Icono_plano_biblioteca.mp3",
                name:"sonido9"
            },{
                url:"Icono_Registro_biblioteca.mp3",
                name:"sonido10"
            },{
                url:"Icono_sala_de_estidio_biblioteca.mp3",
                name:"sonido11"
            },{
                url:"Icono_Sala_de_lectura.mp3",
                name:"sonido12"
            },{
                url:"Icono_Seccion_general_biblioteca.mp3",
                name:"sonido13"
            },{
                url:"Icono_zona_de_consulta_biblioteca.mp3",
                name:"sonido14"
            }

        ];



        if(indexaudio==14){
            onload();
        }else{
            window[audios[indexaudio].name] = new Howl({
                src: [folder+audios[indexaudio].url]
            });

            window[audios[indexaudio].name].once('load', function(){
                indexaudio+=1;
                console.log("Cargado "+indexaudio);
                curso.audio(onload);
            });

        }
    }
    hide(){
        $(ST+"bt3").hide();
        $(ST+"lab").hide();
        $(ST+"box_2").hide();
        //resaltes//
        $(".resalt1,"+ST+"r1,.btn-edi1,.btn-edi2,.btn-edi3").hide().addClass("animated infinite flash");
        $(ST+"r_4,"+ST+"r_1,"+ST+"r_2,"+ST+"r_3,"+ST+"Rectangle4").hide().addClass("animated infinite flash");
        
        $(ST+"resalt1,"+ST+"resalt2,"+ST+"resalt3,"+ST+"resalt4,"+ST+"resalt5").hide().addClass("animated infinite flash");
        $(ST+"resalt1-F,"+ST+"resalt2-F,"+ST+"resalt3-F,"+ST+"resalt4-F,"+ST+"resalt5-F").hide();
        $(ST+"home").hide().addClass("animated infinite rubberBand");
        $(ST+"close-icon2,"+ST+"close-icon3,"+ST+"close-iconM,"+ST+"cerrar").hide();
        $(".icons").hide();
    } 
    play(id,time){
        window[id].timeScale(time);
        window[id].play();
        if(depuracion){console.log("animacion play :"+id+" tiempo: "+time);}
    }

   
    
    reverse(id,time){
        if(depuracion){console.log("animacion reverse:"+id+" tiempo: "+time);}
        window[id].timeScale(time);
        window[id].reverse();   
    }
    
    labels(){
   
        $(".close").attr("title","Cerrar");
        $(ST+"rp1").attr("title","Piso 1: Características");
        $(ST+"rp2").attr("title","Piso 2: Comentario específico");
        $(ST+"rp3").attr("title","Piso 3: Comentario analítico");
        $(ST+"rp4").attr("title","Piso 4: Comentario crítico");
        $(ST+"rp5").attr("title","Piso 5: A modo de ejemplo");
        
        $(ST+"rp_1").attr("title","Piso 1: Características");
        $(ST+"rp_2").attr("title","Piso 2: Referencias bibliográficas");
        $(ST+"rp_3").attr("title","Piso 3: Introducción");
        $(ST+"rp_4").attr("title","Piso 4: El comento");
        $(ST+"rp_5").attr("title","Piso 5: La evaluación");
        $(ST+"rp_6").attr("title","Piso 6: Identificación del autor");
        $(ST+"rp_7").attr("title","Piso 7: Ejemplo");
        
        $(ST+"rp-1").attr("title","Piso 1: Características");
        $(ST+"rp-2").attr("title","Piso 2: Lenguaje");
        $(ST+"rp-3").attr("title","Piso 3: Temáticas");
        $(ST+"rp-4").attr("title","Piso 4: Estructura");
        $(ST+"rp-5").attr("title","Piso 5: Estilo");
        $(ST+"rp-6").attr("title","Piso 6: Ensayistas destacados");
        $(ST+"rp-7").attr("title","Piso 7: Ejemplo");
        
        $(ST+"btn--1").attr("title","Bases");
        $(ST+"btn--2").attr("title","Torre 1: Artículo científico");
        $(ST+"btn--3").attr("title","Torre 2: Monografía");
        $(ST+"btn--4").attr("title","Torre 3: Tesis");
        
        $(ST+"c_1").attr("title","1 Ingreso");
        $(ST+"c_2").attr("title","2 Registro");
        $(ST+"c_3").attr("title","3 Información");
        $(ST+"c_4").attr("title","4 Plano");
        $(ST+"c_5").attr("title","5 Sección general");
        $(ST+"c_6").attr("title","6 Sala de lectura");
        $(ST+"c_7").attr("title","7 Sala de estudio");
        $(ST+"c_8").attr("title","8 Hemeroteca");
        $(ST+"c_9").attr("title","9 Zona de consulta");
        $(ST+"c_10").attr("title","10 Ejemplos");
        
        $(ST+"resalt1,"+ST+"re-1").attr("title","Edificio 1");
        $(ST+"resalt2,"+ST+"re-2").attr("title","Edificio 2");
        $(ST+"resalt3,"+ST+"re-3").attr("title","Edificio 3");
        $(ST+"resalt4,"+ST+"re-4").attr("title","Edificio 4");
        $(ST+"resalt5,"+ST+"re-5").attr("title","Edificio 5");
        
        $(ST+"re-1").on({click:function(){
            $(ST+"a1,"+ST+"a2,"+ST+"a3,"+ST+"a4,"+ST+"a5").css("opacity",1);
            onplay[1]=false;onplay[2]=false;onplay[3]=false;onplay[4]=false;onplay[1]=false;onplay[5]=false;
        }});
        $(ST+"re-2").on({click:function(){
            $(ST+"a1,"+ST+"a2,"+ST+"a3,"+ST+"a4,"+ST+"a5").css("opacity",1);
            onplay[1]=false;onplay[2]=false;onplay[3]=false;onplay[4]=false;onplay[1]=false;onplay[5]=false;
        }});
        $(ST+"re-3").on({click:function(){
            $(ST+"a1,"+ST+"a2,"+ST+"a3,"+ST+"a4,"+ST+"a5").css("opacity",1);
            onplay[1]=false;onplay[2]=false;onplay[3]=false;onplay[4]=false;onplay[1]=false;onplay[5]=false;
        }});
        $(ST+"re-4").on({click:function(){
            $(ST+"a1,"+ST+"a2,"+ST+"a3,"+ST+"a4,"+ST+"a5").css("opacity",1);
            onplay[1]=false;onplay[2]=false;onplay[3]=false;onplay[4]=false;onplay[1]=false;onplay[5]=false;
        }});
        $(ST+"re-5").on({click:function(){
            $(ST+"a1,"+ST+"a2,"+ST+"a3,"+ST+"a4,"+ST+"a5").css("opacity",1);
            onplay[1]=false;onplay[2]=false;onplay[3]=false;onplay[4]=false;onplay[1]=false;onplay[5]=false;
        }});
    }
    links(){
        var link = [{
          id:ST+"pdf",    
          title:'A modo de ejemplo - Descargar',
          url:"http://udecvirtual.unicundi.edu.co/cursos/escritura_academica/documentos/comentario_texto.pdf",
          type:"download"    
        },{
          id:ST+"pdf2",    
          title:'Reseña - Descargar',
          url:"http://udecvirtual.unicundi.edu.co/cursos/escritura_academica/documentos/ejemplo_resena.pdf",
          type:"download"
        },{
          id:ST+"bt1",    
          title:'Ejemplo de reseña 1',
          url:"http://mcv.revues.org/5405",
          type:"page"    
        },{
          id:ST+"bt2",    
          title:'Ejemplo de reseña 2',
          url:"http://www.revistaliteratura.uchile.cl/index.php/RCL/article/viewFile/44359/46368",
          type:"download"    
        },{
          id:ST+"bt3",    
          title:'Ejemplo de reseña 3',
          url:"http://reined.webs.uvigo.es/ojs/index.php/reined/article/viewFile/1244/389",
          type:"download"    
        },{
          id:ST+"pdf3",    
          title:'Ensayos - Descargar',
            url:"https://virtual.ucundinamarca.edu.co/cursos/escritura_academica/documentos/ejemplos_de_ensayo.pdf", type:"download"
        },{
          id:ST+"bt_1",    
          title:'Ejemplo de ensayo 1',
          url:"http://www3.gobiernodecanarias.org/medusa/ecoescuela/clubdelectura/files/2013/08/La+Civilizacion+Del+Espectaculo.pdf",
          type:"download"
        },{
          id:ST+"bt_2",    
          title:'Ejemplo de ensayo 2',
          url:"http://www.mecd.gob.es/dctm/revista-de-educacion/articulosre260/re26005.pdf?documentId=0901e72b813e8e14",
          type:"download"
        },{
          id:ST+"bt_3",    
          title:'Ejemplo de ensayo 3',
          url:"http://www.elespectador.com/noticias/economia/capitalismo-o-socialismo-tertium-non-datur-articulo-482617",
          type:"page"   
        },{
          id:ST+"bt__1",    
          title:'Ejemplo de artículo 1',
          url:"https://dialnet.unirioja.es/descarga/articulo/4714324.pdf",
          type:"download"
        },{
          id:ST+"bt__2",    
          title:'Ejemplo de artículo 2',
          url:"http://www.revistaeducacion.educacion.es/re352/re352_04.pdf",
          type:"download"
        },{
          id:ST+"bt__3",    
          title:'Ejemplo de artículo 3',
          url:"https://revistas.uam.es/tendenciaspedagogicas/article/view/2137/2229",
          type:"page"   
        },{
          id:ST+"bt--1",    
          title:'Ejemplo de monografía 1',
          url:"http://www.javeriana.edu.co/biblos/tesis/politica/tesis123.pdf",
          type:"download"
        },{
          id:ST+"bt--2",    
          title:'Ejemplo de monografía 2',
          url:"http://bibliotecadigital.udea.edu.co/bitstream/10495/5526/1/RamosVald%c3%a9sD_2016_Ranch%c3%b3nPatrimonoArquitectonico.pdf",
          type:"download"
        },{
          id:ST+"bt--3",    
          title:'Ejemplo de monografía 3',
          url:"http://repositori.upf.edu/bitstream/handle/10230/27673/Puig_2016.pdf?sequence=1",
          type:"download"   
        },{
          id:ST+"bt-_1",    
          title:'Ejemplo de tesis 1',
          url:"http://www.bdigital.unal.edu.co/11840/1/41936905.2013.pdf",
          type:"download"
        },{
          id:ST+"bt-_2",    
          title:'Ejemplo de tesis 2',
          url:"https://repository.eafit.edu.co/bitstream/handle/10784/9174/DenisAlberto_CastroRodriguez_Darwin_AramburoPalacios_2016.pdf?sequence=2&isAllowed=y",
          type:"download"
        },{
          id:ST+"bt-_3",    
          title:'Ejemplo de tesis 3',
          url:"http://repository.urosario.edu.co/bitstream/handle/10336/12574/La%20institucionalizaci%c3%b3n%20de%20la%20lucha%20contra%20el%20terrorismo.pdf?sequence=1&isAllowed=y",
          type:"download"
        },{
          id:ST+"bt---1",    
          title:'Ejemplo 1 de textos académicos',
          url:"https://repository.javeriana.edu.co/bitstream/handle/10554/19268/BaronRuizOscar%20avier2015.pdf?sequence=1&isAllowed=y",
          type:"download"
        },{
          id:ST+"bt---2",    
          title:'Ejemplo 2 de textos académicos',
          url:"http://repository.ut.edu.co/bitstream/001/1015/1/RIUT-BHA-spa-2014-La%20exclusi%C3%B3n%20educativa%20de%20los%20estudiantes%20con%20discapacidad%20de%20la%20jurisdicci%C3%B3n%20de%20Veracruz%20en%20el%20municipio%20de%20Alvarado%20%E2%80%93%20Tolima.pdf",
          type:"download"
        },{
          id:ST+"bt---3",    
          title:'Ejemplo 3 de textos académicos',
          url:"http://www.bdigital.unal.edu.co/11155/1/539568.2013.pdf",
          type:"download"
        }]
        link.map(function(item) {
            $(item.id).css("cursor","pointer");
            $(item.id).attr("title",item.title);
            $(item.id).on({click:function(){
                if(item.type=="download"){
                    downloadFile(item.url);    
                }else{
                    window.open(item.url,"UdeC","width=500,height=600"); 
                }
               
            }})
        });
    }
    JSON(){
       var uri = "json/textos.json";
       $.ajax({
           url: uri, 
           dataType: "json", 
           success: function (source) {
              var i = source.textos.length;
              for( var j=0; j<i; j++){
                  $(ST+source.textos[j].btn).attr("data-html",source.textos[j].html);
                  $(ST+source.textos[j].btn).attr("data-edificio",source.textos[j].edificio);
                  $(ST+source.textos[j].btn).on({click:function(){
                        $(ST+"contenidoJSON"+$(this).attr("data-edificio")).html($(this).attr("data-html")); 
                      $(ST+"contenidoJSON"+$(this).attr("data-edificio")).css({"line-height":"23px"})
                  }});
                  
              }
           }, 
           error: function (dato) {
               alert("Ups el Ova no cargado correctamente revise su conexion");
           }
       });
    }
    setInfo(){
        var title="UdeC Escritura Módulo 1";
        var urlicon="http://www.unicundi.edu.co/templates/ah-68-flexi/favicon.ico";
        document.title=title;
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = urlicon;
        document.getElementsByTagName('head')[0].appendChild(link);
    }
    constructor(sym) {
        self=this;
        this.setInfo();
        this.links();
        this.hide();
        this.labels();
        this.JSON();
        this.sym = sym;
      
    }
    history_board(){
        historial = new  History_navigation();
       // historial.get_value();
    }
    animaciones() {
        
        ciudadela = new TimelineMax();
        ciudadela.append(TweenMax.from($(ST+"ciudadela"), .5, {opacity:0}), 0); 
        ciudadela.stop();
        //intro//
        st1 = new TimelineMax({
            onStart:function(){
                ciudadela.play();
            },
            onComplete:function(){
               // setTimeout(function(){
                    self.reverse("st1",100);
               //},2000);
            },
            onReverseComplete:function(){
                if(historial.get_value("nav2")<11){
                    st2.play();    
                }else{
                    st61.play();
                    if(historial.get_value("nav3")==6){
                         $(ST+"lab").show();
                         $(ST+"box1").hide();
                         $(ST+"box_2").show();
                        $(".rf").css("visibility","hidden");
                    }
                }
                
            }
        });
        st1.append(TweenMax.from($(ST+"stage1"), .5, {opacity:0}), 0);     
        st1.append(TweenMax.from($(ST+"avatar"), .5, {x:700,opacity:0}), 0);     
        st1.append(TweenMax.to($(ST+"avatar"), .5, {x:-700,opacity:0,delay:2}), 0);     
        st1.append(TweenMax.from($(ST+"academica"), .5, {x:700,opacity:0}), 0);     
        st1.append(TweenMax.from($(ST+"tema"), .5, {x:700,opacity:0}), 0);     
        st1.append(TweenMax.from($(ST+"tema"), .2, {x:-700,opacity:0,delay:2}), 0);
        st1.stop();
        
        //menu//
        st2 = new TimelineMax({onComplete:function(){
            $(ST+"Recurso_"+historial.get_value("nav1")).show();
        }});
        st2.append(TweenMax.fromTo(ST+"stage2", 1, {x:-900,opacity:0, ease: Elastic.easeOut.config(1, 0.3),  }, {x:0,opacity:1}), 0);   
        st2.append(TweenMax.staggerFrom(".btn1",.4, {x:1500,rotation:900},.2), 0);  
        st2.stop();
        
        //video//
        st3 = new TimelineMax({onStart:function(){
            $(ST+"personaje-2").show();
        }});
        st3.append(TweenMax.to(ST+"stage2", .2, {x:-900,opacity:0 }), 0);   
        st3.append(TweenMax.from(ST+"stage3", .4, {x:-900, ease: Elastic.easeOut.config(1, 0.3)}), 0);
        st3.append(TweenMax.from(ST+"close-icon", .4, {scale:0,rotation:900}), 0);
        st3.stop();
        
        //reto//
        st4 = new TimelineMax();
        st4.append(TweenMax.to(ST+"stage2", .2, {x:-900,opacity:0 }), 0);   
        st4.append(TweenMax.from(ST+"stage4", .4, {x:1200, ease: Elastic.easeOut.config(1, 0.3)}), 0);  
        st4.append(TweenMax.from(ST+"persnoaje-4", .4, {x:1200}), 0);  
        st4.stop();
        
        //para el camino//
        st5 = new TimelineMax({
        onComplete:function(){
            $(ST+"r1").show();
            $(ST+"btn-"+historial.get_value("nav2")).Shadowfollow({x:-11,y:-10,element:ST+"r1"});
        },
            onReverseComplete:function(){
            $(ST+"close-iconM").show();
        }});
        st5.append(TweenMax.to(ST+"stage2", .2, {x:-900,opacity:0 }), 0);   
        st5.append(TweenMax.from(ST+"stage5", .4, {x:1200, ease: Elastic.easeOut.config(1, 0.3)}), 0);  
        st5.append(TweenMax.from(ST+"persnoaje-4Copy", .4, {x:1200}), 0);  
        st5.stop();
        
        //final intro//
        st6 = new TimelineMax({onComplete:function(){
            st61.play();
        }});
        st6.append(TweenMax.to(ST+"stage2", .2, {x:-900,opacity:0 }), 0);   
        st6.append(TweenMax.from(ST+"stage6", .6, {y:-1200,display:"none"}), 0);  
        st6.append(TweenMax.from(ST+"v61", .6, {opacity:0,y:1200,rotationY:900}), 0);  
        st6.append(TweenMax.to(ST+"v61", .2, {opacity:0,x:1200,delay:2}), 0);
        st6.append(TweenMax.from(ST+"v62", .6, {x:1200,opacity:0}), 0);  
        st6.append(TweenMax.to(ST+"v62", .2, {opacity:0,x:1200,delay:2}), 0);
        st6.append(TweenMax.to(ST+"telon-2", .2, {opacity:0,x:1200}), 0);
        st6.stop();
        
        st61 = new TimelineMax({onStart:function(){
            $(ST+"nuves1").hide();
            
        },onComplete:function(){

           var go3=historial.get_value("nav3");
           console.log(go3);    
           Tools.smeller(ST+"resalt$",go3).minor.css("background-image","none");
           
           Tools.smeller(ST+"resalt$",go3).all.show();
           $(ST+"resalt"+go3+"-F").show();
            
           Tools.smeller(ST+"resalt$-F",go3).minor.html("");

            
           
        }});
        st61.append(TweenMax.from(ST+"v63", .6, {x:1200,opacity:0}), 0);  
        st61.append(TweenMax.from(ST+"box1", 1, {x:350,  ease: Elastic.easeOut.config(1, 0.3),delay:.4}), 0);  
        st61.append(TweenMax.from(ST+"Temario", .6, {display:"none"}), 0);  
        st61.append(TweenMax.from(ST+"nuves-2", .6, {display:"none"}), 0);  
        st61.stop();
        
        // Añadimos un método estático como atributo de la "clase" Saludator

   
        //edificio 1//
        st7 = new TimelineMax({onComplete:function(){
            var go4=historial.get_value("nav4");
            $(".labels1").css("opacity",.4);
            Tools.smeller(ST+"rp$",go4).all.show();
            Tools.smeller(ST+"rp$",go4).minor.css("background-image","none");
            Tools.smeller(ST+"l$",go4).all.css("opacity",1);
           
           
        }});
        st7.append(TweenMax.from(ST+"stage7", .6, {y:-1200,display:"none"}), 0);  
        st7.append(TweenMax.staggerFrom(".tree1", .6, {scaleY:0},.4), 0);  
        st7.append(TweenMax.from(ST+"persnoaje-42fw", .6, {x:-1200,onComplete:function(){
            if(historial.get_value("nav3")>1){$(ST+"home").show();}
            sonido2.play(); 
        }}), 0);
        st7.append(TweenMax.staggerFrom([ST+"rp1",ST+"rp2",ST+"rp3",ST+"rp4",ST+"rp5"], .1 , {scale:0},.0), 0);
        st7.append(TweenMax.staggerFrom(".labels1", .4, {scaleX:0,ease: Back.easeOut.config(1.7)},.4), 0);
        
        

        st7.append(TweenMax.from(ST+"a1", .6, {scale:0,rotation:900}), 0);
        st7.stop();
        
        //edificio 2//
        st8 = new TimelineMax({onComplete:function(){
            
            var go9=historial.get_value("nav9");
            $(".labels2").css("opacity",.4);
          
            Tools.smeller(ST+"rp_$",go9).all.show();
            Tools.smeller(ST+"rp_$",go9).minor.css("background-image","none");
            Tools.smeller(ST+"b_$",go9).all.css("opacity",1);
            
        }});
        st8.append(TweenMax.from(ST+"stage8", .6, {y:-1200,display:"none"}), 0);  
        st8.append(TweenMax.staggerFrom(".tree2", .6, {scaleY:0},.4), 0);  
        st8.append(TweenMax.from(ST+"persnoaje-421", .6, {x:-1200}), 0);
       
        st8.append(TweenMax.staggerFrom(".labels2", .4, {scaleX:0,ease: Back.easeOut.config(1.7)},.4), 0);  
        st8.append(TweenMax.from(ST+"a2", .6, {scale:0,rotation:900,onStart:function(){
              if(historial.get_value("nav3")>2){$(ST+"home").show();}
            sonido1.play();
        }}), 0);
        st8.append(TweenMax.staggerFrom([ST+"rp_1",ST+"rp_2",ST+"rp_3",ST+"rp_4",ST+"rp_5",ST+"rp_6",ST+"rp_7"], .1 , {scale:0},.0), 0);
        st8.stop();
        
        //edificio 3//
        st9 = new TimelineMax({onComplete:function(){
            $(".labels3").css("opacity",.4);
          //  sonido3.play();
            var go10=historial.get_value("nav10");
            
            Tools.smeller(ST+"rp-$",go10).all.show();
            Tools.smeller(ST+"rp-$",go10).minor.css("background-image","none");
            Tools.smeller(ST+"bb_$",go10).all.css("opacity",1);
            
        }});
        st9.append(TweenMax.from(ST+"stage9", .6, {y:-1200,display:"none"}), 0);  
        st9.append(TweenMax.staggerFrom(".tree3", .6, {scaleY:0},.4), 0);  
        st9.append(TweenMax.from(ST+"persnoaje-421Copy", .6, {x:-1200}), 0);
        

        st9.append(TweenMax.staggerFrom(".labels3", .4, {scaleX:0,ease: Back.easeOut.config(1.7)},.4), 0);  
        st9.append(TweenMax.from(ST+"a3", .6, {scale:0,rotation:900,onStart:function(){
            if(historial.get_value("nav3")>3){$(ST+"home").show();}
             sonido3.play();
        }} ), 0);
        st9.append(TweenMax.staggerFrom([ST+"rp-1",ST+"rp-2",ST+"rp-3",ST+"rp-4",ST+"rp-5",ST+"rp-6",ST+"rp-7"], .1 , {scale:0},.0), 0);
        st9.stop();
        
        //edificio 4//
        st10 = new TimelineMax({onStart:function(){
                              
                               },onComplete:function(){
            
            var go11=historial.get_value("nav11");  
            $(ST+"r_"+go11).show();
            
            Tools.smeller(ST+"r_$",go11).minor.hide();
            Tools.smeller(ST+"r_$",go11).minor.css("background-image","none");
            $(ST+"btn--"+go11).Shadowfollow(window["edifx"+go11]);
            
        }});
        st10.append(TweenMax.from(ST+"stage10", .6, {y:-1200,display:"none", onStart:function(){ if(historial.get_value("nav3")>4){$(ST+"home").show();}  } }), 0);  
        st10.append(TweenMax.staggerFrom(".tree4", .6, {scaleY:0},.4), 0);  
        st10.append(TweenMax.from(ST+"persnoaje-421Copy2", .6, {x:-1200}), 0);
        st10.append(TweenMax.from(ST+"box--2", .6, {x:-1200}), 0);
        st10.append(TweenMax.from(ST+"btn--1", .6, {y:1200}), 0);
       

        st10.append(TweenMax.from(ST+"bajaA", .6, {opacity:0}), 0);
         st10.append(TweenMax.from(ST+"a4", .6, {scale:0,rotation:900,onStart:function(){
            sonido5.play();
        }}), 0);
        st10.stop();
        
        //edificio 5//
        zoom = new TimelineMax({ onStart:function(){
            $(ST+"red1").hide();},onComplete:function () {
            $(ST+"a5").hide();
            setTimeout(function(){
                $(ST+"red1").show();
            },2400); 
        }});
        zoom.append(TweenMax.to(ST+"nuves5", .001, {display:"none"}), 0);  
        zoom.append(TweenMax.to(ST+"zoom1", .7, {scale:1.5,x:-250,y:-145}), 0);
        zoom.append(TweenMax.to(ST+"persnoaje-421Copy3", .7, {scale:.7,x:-42,y:0}), 0);
        zoom.append(TweenMax.to(ST+"a5", .6, {scale:.7,x:-75,y:60}),-0.7);

        zoom.stop();
        
        st11 = new TimelineMax({
        onStart:function(){
            $(ST+"red1").hide();
            $(".bzoom").hide();
        },
        onComplete:function(){
          $(".bzoom").show(); 
          $(ST+"Rectangle4").show();
          sonido4.play();
          ultimo="sonido4";
          $(ST+"a5").show();
        }
        });
        st11.append(TweenMax.from(ST+"stage11", .6, {y:-1200,display:"none"}), 0);  
        st11.append(TweenMax.staggerFrom(".tree5", .6, {scaleY:0},.4), 0);  
        st11.append(TweenMax.from(ST+"persnoaje-421Copy3", .6, {x:-1200}), 0);
        st11.append(TweenMax.from(ST+"box--3", .6, {x:-1200}), 0);
        st11.append(TweenMax.from(ST+"a5", .6, {scale:0,rotation:900,y:900}), 0);

        st11.append(TweenMax.from(ST+"f", .6, {y:1200}), 0);
        $(ST+"zoome1").on({click:function(){
            sonido4.stop();

            $(".bzoom,"+ST+"box--3").hide();
            self.play("zoom",1);
            self.play("st11Comp1",1);
          
        }});
        st11.stop();
        
        st11Comp1 = new TimelineMax({onComplete:function(){
                if(historial.get_value("nav12")>10){$(ST+"home").show();}
                $(ST+"c_"+historial.get_value("nav12")).Shadowfollow({x:-9,y:-9,element:ST+"red1"});
                if(historial.get_value("nav3")==6){$(ST+"home").show();}
        }});
        st11Comp1.append(TweenMax.staggerFrom(".nums", .6, {rotation:8000,x:1200,y:800},.4), 0);  
        st11Comp1.stop();
        
       
        st12 = new TimelineMax({onComplete:function(){
            setTimeout(function(){
                $(ST+"box1").hide();
                $(ST+"box_2").show();
                self.reverse("st12",10000);
                self.reverse("zoom",1000000);
                self.reverse("st11Comp1",10000);
            },3000);
        },onReverseComplete:function(){
            $(ST+"lab").show();
        }});
        st12.append(TweenMax.to(ST+"v63", .1, {display:"none"}),0); 
        st12.append(TweenMax.from(ST+"stage12", .6, {y:-1200,display:"none"}), 0); 
        st12.append(TweenMax.from(ST+"v121", .6, {opacity:0,y:1200,rotationY:900}), 0);  
        st12.append(TweenMax.to(ST+"v121", .2, {opacity:0,x:1200,delay:2}), 0);
        st12.append(TweenMax.from(ST+"v122", .6, {x:1200,opacity:0}), 0);  
        st12.append(TweenMax.to(ST+"v122", .2, {opacity:0,x:1200,delay:2}), 0);
        st12.stop(); 
        
        credito = new TimelineMax();
        credito.append(TweenMax.from(ST+"creditos", .1, {x:1200}),0); 
        credito.stop();
        var id=1;

        piso51.eventCallback("onComplete", function() {
            //ingreso
            sonido8.play();
            ultimo="sonido8";
            $(ST+"a5").show();
        });
        piso52.eventCallback("onComplete", function() {
            //registro
            sonido10.play();
            ultimo="sonido10";
            $(ST+"a5").show();
        });
        piso53.eventCallback("onComplete", function() {
            //informacion
            sonido7.play();
            ultimo="sonido7";
            $(ST+"a5").show();
        });
        piso54.eventCallback("onComplete", function() {
            //plano
            sonido9.play();
            ultimo="sonido9";
            $(ST+"a5").show();
        });
        piso55.eventCallback("onComplete", function() {
            //s general
            sonido13.play();
            ultimo="sonido13";
            $(ST+"a5").show();
        });
        piso56.eventCallback("onComplete", function() {
            //rsala lectura
            sonido12.play();
            ultimo="sonido12";
            $(ST+"a5").show();
        });
        piso57.eventCallback("onComplete", function() {
            //sala estudio
            sonido11.play();
            ultimo="sonido11";
            $(ST+"a5").show();
        });
        piso58.eventCallback("onComplete", function() {
            //Hemeroteca
            sonido6.play();
            ultimo="sonido6";
            $(ST+"a5").show();
        });
        piso59.eventCallback("onComplete", function() {
            //Zona de consulta
            sonido14.play();
            ultimo="sonido14";
            $(ST+"a5").show();
        });

        piso510.eventCallback("onComplete", function() {
            //Ejemplos
           // sonido13.play();
        //    ultimo="sonido13";
            $(ST+"a5").hide();
        });


        window["st"+id].play();
        return true;
    }
    eventos(sym) {
        sonido2.on('play', function(){
            sym.getSymbol("voca1").play();
        });
        sonido2.on('end', function(){
            onplay[1]=true;
            sym.getSymbol("voca1").play("stop");
            $(ST+"a1").css("opacity",.7);
        });
        sonido2.on('pause', function(){
            sym.getSymbol("voca1").play("stop");
        });
        sonido2.on('stop', function(){
            sym.getSymbol("voca1").play("stop");
        });
        
        
        
        sonido6.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido7.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido8.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido9.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido10.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido11.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido12.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido13.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
        sonido14.on('end', function(){
            $(ST+"a5").css("opacity",.7);
            onplay[5]=true;
        });
      
        
        

        sonido1.on('play', function(){
            sym.getSymbol("voca2").play();
        });
        sonido1.on('end', function(){
            onplay[2]=true;
            sym.getSymbol("voca2").play("stop");
            $(ST+"a2").css("opacity",.7);
        });
        sonido1.on('pause', function(){
            sym.getSymbol("voca2").play("stop");
        });
        sonido1.on('stop', function(){
            sym.getSymbol("voca2").play("stop");
        });

        sonido3.on('play', function(){
            sym.getSymbol("voca3").play();
        });
        sonido3.on('end', function(){
            onplay[3]=true;
            sym.getSymbol("voca3").play("stop");
            $(ST+"a3").css("opacity",.7);
        });
        sonido3.on('pause', function(){
            sym.getSymbol("voca3").play("stop");
        });
        sonido3.on('stop', function(){
            sym.getSymbol("voca3").play("stop");
        });

        sonido4.on('play', function(){
            sym.getSymbol("voca5").play();
        });
        sonido4.on('end', function(){
            sym.getSymbol("voca5").play("stop");
        });
        sonido4.on('pause', function(){
            sym.getSymbol("voca5").play("stop");
        });
        sonido4.on('stop', function(){
            sym.getSymbol("voca5").play("stop");
        });

        sonido5.on('play', function(){
            sym.getSymbol("voca4").play();
        });
        sonido5.on('end', function(){
            $(ST+"a4").css("opacity",.7);
             onplay[4]=true;
            sym.getSymbol("voca4").play("stop");
        });
        sonido5.on('pause', function(){
            sym.getSymbol("voca4").play("stop");
        });
        sonido5.on('stop', function(){
            sym.getSymbol("voca4").play("stop");
        });

        for (var i=6;i<=14;i++){
            window["sonido"+i].on('play', function(){
                sym.getSymbol("voca5").play();
            });
            window["sonido"+i].on('end', function(){
                sym.getSymbol("voca5").play("stop");
            });
            window["sonido"+i].on('pause', function(){
                sym.getSymbol("voca5").play("stop");
            });
            window["sonido"+i].on('stop', function(){
                sym.getSymbol("voca5").play("stop");
            });
        }


        $(ST+"a1").on({click:function () {
            if(onplay[1]==true){$(this).css("opacity",1);onplay[1]=false;sonido2.play();}else{onplay[1]=true;sonido2.pause();$(this).css("opacity",.7);}
        }});
        $(ST+"a2").on({click:function () {
            if(onplay[2]==true){$(this).css("opacity",1);onplay[2]=false;sonido1.play();}else{onplay[2]=true;sonido1.pause();$(this).css("opacity",.7);}
        }});
        $(ST+"a3").on({click:function () {
            if(onplay[3]==true){$(this).css("opacity",1);onplay[3]=false;sonido3.play();}else{onplay[3]=true;sonido3.pause();$(this).css("opacity",.7);}
        }});
        $(ST+"a4").on({click:function () {
            if(onplay[4]==true){$(this).css("opacity",1);onplay[4]=false;sonido5.play();}else{onplay[4]=true;sonido5.pause();$(this).css("opacity",.7);}
        }});
        $(ST+"a5").on({click:function () {
            if(onplay[5]==true){
                $(this).css("opacity",1);
                onplay[5]=false;
                window[ultimo].play();
            }else{
                onplay[5]=true;
                window[ultimo].pause();
                $(this).css("opacity",.7);
            }
        }});
        //insert video//
        $(ST+"video").html('<iframe id="popup-youtube-player" width="100%" height="100%" src="//www.youtube.com/embed/-8ef2WcdvcE?enablejsapi=1&version=3&playerapiid=ytplayer&rel=0" frameborder="0" allowfullscreen></iframe>');
        //botones del menu//
        nav1 = $([ST+"icono1",ST+"icono2",ST+"icono3"]).LinearNav({
            onClick:function(obj,excludes,n,settings){
               var h1=n+1;
               if(h1!=4){ historial.set_value("nav1",h1); }
               switch(n) {
                    case 1:
                        st3.play();
                        if(settings.ln==1){
                            $(ST+"Recurso_1").hide();
                            $(ST+"Recurso_2").show();    
                        }   
                        break;
                    case 2:
                        st4.play();
                        if(settings.ln==2){
                            $(ST+"Recurso_2").hide();
                            $(ST+"Recurso_3").show();
                        }
                        break;
                    case 3:
                        st5.play();
                        if(settings.ln==3){
                            $(ST+"Recurso_3").hide();
                        }
                        break;   
                } 
            }
        });
        
      
        //cerrar video//
        $(ST+"close-icon").on({
            click:function(){
                $('#popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                self.reverse("st3",3000);
            }
        });
        
        //slider reto//
        $(ST + "cont1").LPSlider({
            N_slides: 2
            , left: S + "b2"
            , right: S + "b1"
            , slides: ".s1"
            , direction: "top"
            , variable: "s_frames"
            , onClick: function () {}
            , onFinish: function () {
                $(ST+"close-icon2").show();
            }
        });
        //cerrar reto//
        $(ST+"close-icon2").on({
            click:function(){
                self.reverse("st4",3000);
            }
        });
        
        //slider para el camino//
        $(ST + "cont2").LPSlider({
            N_slides: 2
            , left: S + "b4"
            , right: S + "b3"
            , slides: ".s2"
            , direction: "top"
            , variable: "s_frames2"
            , onClick: function () {}
            , onFinish: function () {
                $(ST+"close-icon3").show();
            }
        });
        //cerrar para el camino//
        $(ST+"close-icon3").on({
            click:function(){
                self.reverse("st5",3000);
            }
        });
        for (var i=1; i<=10; i++){
            popUp1(i,"box"+i,ST+"c"+i,ST+"cl"+i);
        }
        //para el camino//
        nav2 = $([ST+"btn-1",ST+"btn-2",ST+"btn-3",ST+"btn-4",ST+"btn-5",ST+"btn-6",ST+"btn-7",ST+"btn-8",ST+"btn-9",ST+"btn-10"]).LinearNav({
            onClick:function(e,o,n){
                window["box"+n].play();
                var newn=n+1;
                historial.set_value("nav2",newn);
             
                $(ST+"btn-"+newn).Shadowfollow({x:-11,y:-10,element:ST+"r1"});
                if(newn==11){historial.set_value("nav1",4); $(ST+"r1").css("visibility","hidden");}
                $(ST+"r1").hide();
            }
            
        });
        //cerrar menu intr0o//
        $(ST+"close-iconM").on({
            click:function(){
                self.reverse("st2",3000);
                if(historial.get_value("nav3")==6){
                    $(ST+"lab").show();
                    $(ST+"v63").show();
                    $(ST+"Temario").show();
                    st61.play();
                    $(ST+"box_2").show();
                    $(ST+"box1").hide();
                }else{
                    
                    st6.play();    
                }
            }
        });
        
        //animacion de apertura de cada piso del edificio 1//
        for (var i=1;i<=5;i++){
            edificio1(i,"edif"+i,ST+"pisillo"+i);
        }
        
        //pisos edificio 1//
        nav4 = $([ST+"rp1",ST+"rp2",ST+"rp3",ST+"rp4",ST+"rp5"]).LinearNav({
            onClick:function(e,o,n){
                if($(ST+"a1").css("opacity")=="1"){
                    $(ST+"a1").trigger("click");
                }
                    $(ST+"home").hide();
                    $(ST+"anima1").css("visibility","visible")
                    $(ST+"cerrar").attr("data-Play",n);
                    $(ST+"titulo1").html($(e).attr("title"));
                    window["edif"+n].play();
                    $(e).css("background-image","none");
                    $(ST+"labs1").hide();
                    $(ST+"persnoaje-42fw").hide();
                    $(ST+"a1").hide();
                    var nn=n+4;
                    var go5=historial.get_value("nav"+nn);
                
                if(n<5){
                    $(ST+"p"+n+go5).Shadowfollow({x:-71,y:10,element:ST+"anima1"}); 
                    if(go5 == 5 && n == 1) {$(ST+"anima1").css("visibility","hidden"); $(ST+"cerrar").show();};
                    if(go5 == 4 && n == 2) {$(ST+"anima1").css("visibility","hidden"); $(ST+"cerrar").show();};
                    if(go5 == 4 && n == 3) {$(ST+"anima1").css("visibility","hidden"); $(ST+"cerrar").show();};
                    if(go5 == 4 && n == 4) {$(ST+"anima1").css("visibility","hidden"); $(ST+"cerrar").show();};
                }else{
                    historial.set_value("nav4",6);
                    historial.set_value("nav3",2);
                    $(ST+"anima1").css("visibility","hidden");
                }
            },
            onComplete:function(){
                $(ST+"cerrar").show();
            }
        });
        
        //item primer piso edificio1//
        for (var i=1;i<=4;i++){
            itemsEdificio1(i,"E1piso1"+i,ST+"pisillo1",ST+"gl"+i);
        }
        nav5 = $([ST+"p11",ST+"p12",ST+"p13",ST+"p14"]).LinearNav({
            onClick1:function(e,otros){
                if(encendidoA1){
                    E1piso11.play();
                    $(ST+"p12").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E1piso11");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav5",2);
                }
            },
            onClick2:function(e,otros){
                if(encendidoA1){
                    E1piso12.play();
                    $(ST+"p13").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E1piso12");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav5",3);
                }
            },
            onClick3:function(e,otros){
                if(encendidoA1){
                    E1piso13.play();
                    $(ST+"p14").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E1piso13");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav5",4);
                }
            },
            onClick4:function(e,otros){
                if(encendidoA1){
                    E1piso14.play();
                    $(ST+"anima1").css({top:1000})
                    $(ST+"cl").attr("data-Play","E1piso14");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav5",5);
                    
                    
                }    
                
            },
            onComplete:function(){
                $(ST+"cerrar").show();
            }
        });
        
        
        $(ST+"cl").on({click:function(){
            $(ST+"p11,"+ST+"p12,"+ST+"p13,"+ST+"p14").css("opacity",1);
            self.reverse($(ST+"cl").attr("data-Play"),1);
        }});
        
        
        //item segundo tercero y cuarto  piso edificio1//
        for (var i=1;i<=3;i++){
            itemsEdificio1(i,"E2piso2"+i,ST+"pisillo2",ST+"gl"+i);
            itemsEdificio1(i,"E3piso3"+i,ST+"pisillo3",ST+"gl"+i);
            itemsEdificio1(i,"E4piso4"+i,ST+"pisillo4",ST+"gl"+i);
        }
        nav6 = $([ST+"p21",ST+"p22",ST+"p23"]).LinearNav({
            onClick1:function(e,otros){
                if(encendidoA1){
                    E2piso21.play();
                    $(ST+"p22").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E2piso21");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav6",2);
                }
            },
            onClick2:function(e,otros){
                if(encendidoA1){
                    E2piso22.play();
                    $(ST+"p23").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E2piso22");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav6",3);
                }
            },
            onClick3:function(e,otros){
                if(encendidoA1){
                    E2piso23.play();
                    $(ST+"anima1").css({top:1000});
                    $(ST+"cl").attr("data-Play","E2piso23");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav6",4);
                  
                }
            },
            onComplete:function(){
                $(ST+"cerrar").show();
            }
        });
        nav7 =  $([ST+"p31",ST+"p32",ST+"p33"]).LinearNav({
            onClick1:function(e,otros){
                if(encendidoA1){
                    E3piso31.play();
                    $(ST+"p32").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E3piso31");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav7",2);
                }
            },
            onClick2:function(e,otros){
                if(encendidoA1){
                    E3piso32.play();
                    $(ST+"p33").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E3piso32");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav7",3);
                }
            },
            onClick3:function(e,otros){
                if(encendidoA1){
                    E3piso33.play();
                    $(ST+"anima1").css({top:1000});
                    $(ST+"cl").attr("data-Play","E3piso33");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav7",4);
                }
            },
            onComplete:function(){
                $(ST+"cerrar").show();
            }
        });
        
        nav8 =   $([ST+"p41",ST+"p42",ST+"p43"]).LinearNav({
            onClick1:function(e,otros){
                if(encendidoA1){
                    E4piso41.play();
                    $(ST+"p42").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E4piso41");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav8",2);
                }
            },
            onClick2:function(e,otros){
                if(encendidoA1){
                    E4piso42.play();
                    $(ST+"p43").Shadowfollow({x:-71,y:10,element:ST+"anima1"});
                    $(ST+"cl").attr("data-Play","E4piso42");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav8",3);
                }
            },
            onClick3:function(e,otros){
                if(encendidoA1){
                    E4piso43.play();
                    $(ST+"anima1").css({top:1000});
                    $(ST+"cl").attr("data-Play","E4piso43");
                    $(otros).css("opacity",.5);
                    encendidoA1=false;
                    historial.set_value("nav8",4);
                }
            },
            onComplete:function(){
                $(ST+"cerrar").show();
            }
        });
        
        
        
        $(ST+"cl").on({click:function(){
             $(".btnEdi1").css("opacity",1);
            self.reverse($(ST+"cl").attr("data-Play"),1);
        }});
        
        //cerrar pisos edi 1//
        $(ST+"cerrar").on({click:function(){
            $(this).hide();
            
            $(".btnEdi1").css("opacity",1);
            try{
                self.reverse("edif"+$(ST+"cerrar").attr("data-Play"),1);
                self.reverse($(ST+"cl").attr("data-Play"),1);
            }catch(err) {
                
            }
            var nid=parseInt($(ST+"cerrar").attr("data-Play"))+1;
            $(ST+"rp"+nid).show();
            historial.set_value("nav4",nid);
            setTimeout(function(){
                  $(ST+"persnoaje-42fw").show();
                  $(ST+"a1").show();
                  $(ST+"labs1").show();
                  var go4=historial.get_value("nav4");
                  $(".labels1").css("opacity",.4);
                  Tools.smeller(ST+"l$",go4).all.css("opacity",1);
                 
                  for(var i=1; i<=nid; i++){ $(ST+"l"+i).css("opacity",1);}
                
                  
                  if(historial.get_value("nav3")>1){
                        $(ST+"home").show();
                  }
                  if($(ST+"cerrar").attr("data-Play")==5){
                      $(ST+"home").show();  
                      $(ST+"resalt2").show();
                      try{
                          $(ST+"resalt1-F").html("");
                          $(ST+"resalt2-F").show();    
                      }
                      catch(err) {
                          console.log("error 2 leve")
                      }
                  }
                   
           
            },1000);
         
            
        }});
        ////////////////////////////////////////////////////////////////////////////////
        for (var i=1;i<=7;i++){
            edificio2(i,"piso2"+i);
        }
        //pisos edificio 2//
        nav9 = $([ST+"rp_1",ST+"rp_2",ST+"rp_3",ST+"rp_4",ST+"rp_5",ST+"rp_6",ST+"rp_7"]).LinearNav({
            onClick:function(e,o,n){
                if($(ST+"a2").css("opacity")=="1"){
                    $(ST+"a2").trigger("click");
                }
                var af=n+1;
                historial.set_value("nav9",af);
                if(af==8){historial.set_value("nav3",3);}
                $(ST+"home").hide();
                if(n>6){
                    $(ST+"conte3").show();
                    $(ST+"conte2").hide();
                }else{
                    $(ST+"conte2").show();
                    $(ST+"conte3").hide();
                }
                $(".labels2").css("opacity",.4);
                  $(ST+"b_"+n).css("opacity",1);
                    $(ST+"persnoaje-421").hide();
                    $(ST+"a2").hide();
                    $(ST+"cerrar2").attr("data-Play",n);
                    $(ST+"titulo2").html($(e).attr("title"));
                    window["piso2"+n].play();
                    $(e).css("background-image","none");
                    $(".labels2").hide();
            },
            onComplete:function(){
                $(ST+"cerrar2").show();
            }
        });
        //cerrar pisos edi 2//
        $(ST+"cerrar2").on({click:function(){
            self.reverse("piso2"+$(ST+"cerrar2").attr("data-Play"),1);
            var nid=parseInt($(ST+"cerrar2").attr("data-Play"))+1;
            $(ST+"rp_"+nid).show();
            setTimeout(function(){
                  $(ST+"persnoaje-421").show();
                $(ST+"a2").show();
                  $(".labels2").show();
                  $(".labels2").css("opacity",.4);
                  var go9=historial.get_value("nav9");
                  Tools.smeller(ST+"b_$",go9).all.css("opacity",1);
                  if(go9==8){$(ST+"home").show(); historial.set_value("nav3",3);}
                  if($(ST+"cerrar2").attr("data-Play")==7){
                      $(ST+"home").show();  
                      $(ST+"resalt3").show();
                      try{
                          $(ST+"resalt2-F").html("");
                          $(ST+"resalt3-F").show();    
                      }
                      catch(err) {
                          console.log("error 3 leve");
                      }
                      
                  }
            },1000);
            
        }});
        
        ////////////////////////////////////////////////////////////////////////////////
        for (var i=1;i<=7;i++){
            edificio3(i,"piso3"+i);
        }
        //pisos edificio 3//
        nav10 = $([ST+"rp-1",ST+"rp-2",ST+"rp-3",ST+"rp-4",ST+"rp-5",ST+"rp-6",ST+"rp-7"]).LinearNav({
            onClick:function(e,o,n){
                if($(ST+"a3").css("opacity")=="1"){
                    $(ST+"a3").trigger("click");
                }
                var af=n+1;
                $(ST+"home").hide();
                historial.set_value("nav10",af);
                if(af==8){historial.set_value("nav3",4);}
                if(n>6){
                    $(ST+"conte5").show();
                    $(ST+"conte4").hide();
                }else{
                    $(ST+"conte4").show();
                    $(ST+"conte5").hide();
                }
                    $(ST+"cerrar3").attr("data-Play",n);
                    $(ST+"titulo3").html($(e).attr("title"));
                    window["piso3"+n].play();
                    $(e).css("background-image","none");
                    $(".labels3").hide();
                    $(ST+"persnoaje-421Copy").hide();
                    $(ST+"a3").hide();
            },
            onComplete:function(){
                $(ST+"cerrar3").show();
            }
        });
        //cerrar pisos edi 3//
        $(ST+"cerrar3").on({click:function(){
            self.reverse("piso3"+$(ST+"cerrar3").attr("data-Play"),1);
            var nid=parseInt($(ST+"cerrar3").attr("data-Play"))+1;
            $(ST+"rp-"+nid).show();
            setTimeout(function(){
                  $(ST+"persnoaje-421Copy").show();
                  $(ST+"a3").show();
                  $(".labels3").show();
                  $(".labels3").css("opacity",.4);
                  $(ST+"bb_"+nid).css("opacity",1);
                  var go10=historial.get_value("nav10");
                  Tools.smeller(ST+"bb_$",go10).all.css("opacity",1);
                  if(go10==8){$(ST+"home").show(); historial.set_value("nav3",4);}
                  if($(ST+"cerrar3").attr("data-Play")==7){
                      $(ST+"home").show();  
                      $(ST+"resalt4").show();
                      try{
                          $(ST+"resalt3-F").html("");
                          $(ST+"resalt4-F").show();    
                      }
                      catch(err) {
                          console.log("error 4 leve");
                      }
                      
                  }
            },1000);
            
        }});
        ///////////////////////////////////////////////////////////////////////////////
        for (var i=1;i<=4;i++){
            edificio4(i,"piso4"+i);
        }
        //pisos edificio 4//
        nav11 = $([ST+"btn--1",ST+"btn--2",ST+"btn--3",ST+"btn--4"]).LinearNav({
            onClick:function(e,o,n){
                    if($(ST+"a4").css("opacity")=="1"){
                        $(ST+"a4").trigger("click");
                    }
                    $(ST+"cerrar4").attr("data-Play",n);
                    $(ST+"titulo4").html($(e).attr("title"));
                    window["piso4"+n].play();
                    $(ST+"r_"+n).hide();
                    $(ST+"persnoaje-421Copy2").hide();
                    var newN=n+1;
                    historial.set_value("nav11",newN);
                    $(ST+"home").hide(); 
                    if(n==4){historial.set_value("nav3",5)}
            },
            onComplete:function(){
                $(ST+"cerrar4").show();
            }
        });
        
        
        //cerrar pisos edi 4//
        $(ST+"cerrar4").on({click:function(){
            $(ST+"persnoaje-421Copy2").show();
            self.reverse("piso4"+$(ST+"cerrar4").attr("data-Play"),10);
            var nid=parseInt($(ST+"cerrar4").attr("data-Play"))+1;
            $(ST+"r_"+$(ST+"cerrar4").attr("data-Play")).css("background-image","none");
            $(ST+"r_"+nid).show();
            $(ST+"btn--"+nid).Shadowfollow(window["edifx"+nid]);
            
            var go11=historial.get_value("nav11");
if(go11==5){$(ST+"home").show(); historial.set_value("nav3",5);}
            setTimeout(function(){
               
               if($(ST+"cerrar4").attr("data-Play")==4){
                   try{
                          $(ST+"resalt4-F").html("");
                          $(ST+"resalt5-F").show();    
                   }
                   catch(err) {
                          console.log("error 5 leve");
                   }
                      
                  
                   $(ST+"bajaA").hide();
                   $(ST+"resalt5").show();
                }
            },1000);     
        }});
        $(ST + "co1").LPSlider({
            N_slides: 2
            , left: S + "ant1"
            , right: S + "sig1"
            , slides: ".co1"
            , direction: "top"
            , variable: "co1"
            , onClick: function () {}
            , onFinish: function () {
                //$(ST+"close-icon3").show();
            }
        });
        
        
        //bolas 5 edi//
        $(ST+"c_1,"+ST+"c_2,"+ST+"c_3,"+ST+"c_4,"+ST+"c_5").on({
                mouseover: function () {
                    
                    $(".icons").hide();
                    var num=$(this).attr("id").split(S+"c_")[1];
                    $(ST+"i_"+num).show();
                    $(ST+"t_1").text($(this).attr("title").replace(num, ""));
                    $(ST+"tool1").show(); 
                    $(this).Shadowfollow({x:-80,y:-80,element:ST+"tool1"});
                }
                ,mouseout: function () {
                    $(ST+"tool1").hide();   
                    $(".icons").hide();
                }
         });
         $(ST+"c_6,"+ST+"c_7,"+ST+"c_8,"+ST+"c_9,"+ST+"c_10").on({
                mouseover: function () {
                    $(".icons").hide();
                    var num=$(this).attr("id").split(S+"c_")[1];
                    $(ST+"i_"+num).show();
                    $(ST+"t_2").text($(this).attr("title").replace(num, ""));
                    $(ST+"tool2").show(); 
                    $(this).Shadowfollow({x:-80,y:110,element:ST+"tool2"});
                }
                ,mouseout: function () {
                    $(".icons").hide();
                    $(ST+"tool2").hide();     
                }
        });
        for (var i=1;i<=10;i++){
            edificio5(i,"piso5"+i);
        }
       
        
        //bolas edificio 5//
       nav12 = $([ST+"c_1",ST+"c_2",ST+"c_3",ST+"c_4",ST+"c_5",ST+"c_6",ST+"c_7",ST+"c_8",ST+"c_9",ST+"c_10"]).LinearNav({
            onClick:function(e,o,n){
                
                $(ST+"home").attr("status",$(ST+"home").css("display"));
                $(ST+"home").hide();
                if (n!=10){
                    $(ST+"conte6").show();
                    $(ST+"conte7").hide();
                }else{
                    
                    $(ST+"conte6").hide();
                    $(ST+"conte7").show();
                }
                $(ST+"cerrar5").attr("data-Play",n);
                $(ST+"titulo5").html($(e).attr("title"));
                window["piso5"+n].play();
                $(ST+"red1").hide();
                var nn=n+1;
                historial.set_value("nav12",nn);
                if(nn==11){ historial.set_value("nav3",6);}
            },
            onComplete:function(){
                $(ST+"cerrar4").show();
            }
        });
        //cerrar pisos edi 5//
        $(ST+"cerrar5").on({click:function(){
            if($(ST+"home").attr("status")=="block"){
                $(ST+"home").show();
            }
            sonido6.stop();
            sonido7.stop();
            sonido8.stop();
            sonido9.stop();
            sonido10.stop();
            sonido11.stop();
            sonido12.stop();
            sonido13.stop();
            sonido14.stop();
            onplay[5]=false;
            $(ST+"a5").css("opacity",1);
            self.reverse("piso5"+$(ST+"cerrar5").attr("data-Play"),1);
            var nid=parseInt($(ST+"cerrar5").attr("data-Play"))+1;
            $(ST+"red1").show();
            $(ST+"c_"+nid).Shadowfollow({x:-9,y:-9,element:ST+"red1"});
            if(nid==11){
                $(ST+"red1").hide();
                setTimeout(function(){
                    if(historial.get_value("nav12")==11) {
                        historial.set_value("nav12",12);
                        self.reverse("st11", 200);
                        st12.play();
                    }
                },3000);
            }
            
        }});
        
        //////////////////////////MAIN PRINCIPAL//////////////////////////////////
        
        
        nav3 =$([ST+"resalt1",ST+"resalt2",ST+"resalt3",ST+"resalt4",ST+"resalt5"]).LinearNav({
            onClick:function(e,o,n){
                historial.set_value("nav3",n);
                switch(n) {
                    case 1:
                        self.play("st7",1);
                        $(ST+"home").attr("data-reverse","st7");
                        $(ST+"resalt1-F").hide();
                        break;
                    case 2:
                        self.play("st8",1);
                        $(ST+"home").attr("data-reverse","st8");
                        $(ST+"resalt2-F").hide();
                        break;
                    case 3:
                        self.play("st9",1);
                        $(ST+"home").attr("data-reverse","st9");
                        $(ST+"resalt3-F").hide();
                        break;
                    case 4:
                        self.play("st10",1);
                        $(ST+"home").attr("data-reverse","st10");
                        $(ST+"resalt4-F").hide();
                        break;  
                    case 5:
                        self.play("st11",1);
                        $(ST+"home").attr("data-reverse","st11");
                        $(ST+"resalt5-F").hide();
                        break;      
                }
                $(e).css("background-image","none");
            }
        }); 
        
        $(ST+"home").on({click:function(){
            sonido1.stop();
            sonido2.stop();
            sonido3.stop();
            sonido4.stop();
            sonido5.stop();
            sonido6.stop();
            sonido7.stop();
            sonido8.stop();
            sonido9.stop();
            sonido10.stop();
            sonido11.stop();
            sonido12.stop();
            sonido13.stop();
            sonido14.stop();
            self.reverse($(ST+"home").attr("data-reverse"),100);
            if($(ST+"home").attr("data-reverse")=="st11"){
                 $(ST+"box1").hide();
                $(ST+"box_2").show();
                self.reverse("st12",10000);
                self.reverse("zoom",1000000);
                self.reverse("st11Comp1",10000);
                 
            }
            if($(ST+"home").attr("data-reverse")=="st10"){
                self.reverse("piso41",10000);
                self.reverse("piso42",10000);
                self.reverse("piso43",10000);
                self.reverse("piso44",10000);
                
                
                $(ST+"persnoaje-421Copy2").show();
                $(ST+"a2").show();
                $(".labels2").show();
                $(ST+"a4").css("opacity",1);
                
                
                 
            }
           
            $(this).hide();
        }});
        
        $(ST+"intro").on({click:function(){
            self.play("st2",1);
            
                $(ST+"lab").hide();
                $(ST+"v63").hide();
                $(ST+"Temario").hide();
                $(ST+"close-iconM").show();
   
        }});
        
        $(ST+"ceditos").on({click:function(){
            self.play("credito",1);
            $(ST+"v63").hide();
             $(ST+"lab").hide();
            
            
        }});
        
        $(ST+"creditos").on({click:function(){
            self.reverse("credito",1);
            $(ST+"v63").show();
            $(ST+"lab").show();
        }});
        
        
        $(".edilson").on({
            click:function(){
            var id=parseInt($(this).attr("id").split(S+"re-")[1]);
                $(ST+"resalt"+id).trigger("click");    
               
            },
            mouseover: function () {
                var id=$(this).attr("id").split(S+"re-")[1];
                console.log(id+">="+nav3.ln);
                if(id<=nav3.ln){
                    $(this).css("cursor","pointer");
                }else{
                    $(this).css("cursor","no-drop");
                }
            }
        });
        
        
        
        
        return true;
    }
}

function popUp1(n,name,box,close){
    window[name]= new TimelineMax({onReverseComplete:function(){
        $(ST+"r1").show();
        window[name].timeScale(1);
       
    }});
    window[name].append(TweenMax.fromTo(ST+"contenedor", .4, {display:"none"},{display:"block"}), 0);  
    window[name].append(TweenMax.from(box, .4, {x:1200}), 0);  
    window[name].append(TweenMax.from(close, .4, {scale:0,opacity:0,rotation:900}), 0);  
    window[name].stop();
    $(close).on({click:function(){
         self.reverse(name,5);
    }})
}
function edificio1(n,name,box){
    window[name]= new TimelineMax({onStart:function(){
        $(ST+"anima1").hide();
    },onComplete:function(){
        $(ST+"anima1").show();
        encendidoA1=true;
    }});
    window[name].append(TweenMax.fromTo(ST+"boxme", .4, {display:"none",x:900},{display:"block",x:0}), 0);  
    window[name].append(TweenMax.from(box, .4, {x:200,display:"none"}), 0);  
    window[name].stop();
}
function edificio2(n,name){
    window[name]= new TimelineMax();
    window[name].append(TweenMax.fromTo(ST+"boxme2", .4, {display:"none",x:900},{display:"block",x:0}), 0);  
    window[name].append(TweenMax.fromTo(ST+"conte2", .4, {opacity:0},{opacity:1}), 0);  
    window[name].stop();
}
function edificio3(n,name){
    window[name]= new TimelineMax();
    window[name].append(TweenMax.fromTo(ST+"boxme3", .4, {display:"none",x:900},{display:"block",x:0}), 0);  
    window[name].append(TweenMax.fromTo(ST+"conte4", .4, {opacity:0},{opacity:1}), 0);  
    window[name].stop();
}
function edificio4(n,name){
    window[name]= new TimelineMax();
    window[name].append(TweenMax.fromTo(ST+"boxme4", .4, {display:"none",x:900},{display:"block",x:0}), 0);  
    window[name].append(TweenMax.fromTo(ST+"text_"+n, .4, {display:"none"},{display:"block"}), 0);  
    window[name].stop();
}
function edificio5(n,name){
    window[name]= new TimelineMax({onReverseComplete:function(){
        $(ST+"a5").hide();
    },onComplete:function(){
        $(ST+"a5").show();
    }});
    window[name].append(TweenMax.fromTo(ST+"Rectangle5", .4, {x:1200},{x:0}), 0);  
    window[name].append(TweenMax.fromTo(ST+"boxme5", .4, {display:"none",x:900},{display:"block",x:0}), 0);  
  
    window[name].append(TweenMax.fromTo(ST+"conte6", .4, {opacity:0},{opacity:1}), 0);  
    window[name].stop();
}
function itemsEdificio1(n,name,menu,globo){
    window[name]= new TimelineMax({onReverseComplete(){
         $(ST+"anima1").show();
         encendidoA1=true;
    },onStart:function(){
         $(ST+"anima1").hide();
    }});
    window[name].append(TweenMax.fromTo(menu, .4,{x:0} ,{x:-165}), 0);  
    window[name].append(TweenMax.fromTo(ST+"conte1", .2, {display:"none"}, {display:"block"}), 0);  
    window[name].append(TweenMax.fromTo([globo,ST+"contenidoJSON1"], .4, {scale:0}, {scale:1}), 0);  
    window[name].stop();
}
var curso="";
function main(sym) {
    curso = new UdeC();

    curso.audio(function(){
        if (curso.eventos(sym)) {
            curso.history_board();
            if (curso.animaciones()) {
                $(ST + "preload").hide();
            }
        }
    });


}