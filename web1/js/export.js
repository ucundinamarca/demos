
var html='<!DOCTYPE html>';
html=html+'<html>';
html=html+'<head>';
html=html+'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
html=html+'<meta http-equiv="X-UA-Compatible" content="IE=Edge" />';
html=html+'<title>Universidad Cundinamarca</title>';
html=html+'<link rel="stylesheet" href="css/style.css">';
html=html+'<script type="text/javascript" charset="utf-8" src="js/TweenMax.min.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/SCORM_API_wrapper.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/TweenMax.min.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/howler.min.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/History_navigation.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/Tools.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/main.js"></script>';
html=html+'<script type="text/javascript" charset="utf-8" src="js/init.js"></script>';
html=html+'</head>';
html=html+'<body style="margin:0;padding:0;">';

function descargarArchivo(contenidoEnBlob, nombreArchivo) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var save = document.createElement('a');
        save.href = event.target.result;
        save.target = '_blank';
        save.download = nombreArchivo || 'archivo.dat';
        var clicEvent = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        save.dispatchEvent(clicEvent);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    };
    reader.readAsDataURL(contenidoEnBlob);
};

function generarTexto(Element) {
    var texto = [];
    texto.push(Element);
    return new Blob(texto, {
        type: 'text/plain'
    });
};

var element = '';
var images=true;
$("div").each(function () {
    if(images==true){
        var image = $(this).css('background-image');
        if (image != "none") {
            $(this).css("width","");
            $(this).css("height","");
            $(this).css('background-image',"");
            $(this).css('background-repeat',"");
            $(this).css('background-size',"");
            $(this).css('background-position',"");
        }
    }
    if($(this).attr("id")!=null){
        element += "#" + $(this).attr("id") + "{" + $(this).attr("style") + "}";
    }else{
        element += "." + $(this).attr("class") + "{" + $(this).attr("style") + "}";
    }
    //se exporta como img en caso de estar en true//
    if(images==true){
        if (image != "none") {
            var id=$(this).attr("id");
            var clase=$(this).attr("class");
            image = image.replace('url(','').replace(')','').replace(/\"/gi, "");
            image =image.replace(document.location.origin+"/","");
            $div = $('<img id="'+id+'" class="'+clase+'"   >').attr("src",image);
            $(this).replaceWith($div);
            $(this).attr("id",id);
        }
    }
});
descargarArchivo(generarTexto(element), 'css.css');
$("div").each(function () {
    $(this).removeAttr("style");
});
descargarArchivo(generarTexto(html+$("body").html()+"</body></html>"), 'index.html');