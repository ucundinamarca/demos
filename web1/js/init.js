var ST2 = "#Stage";
$(function () {
    $(ST2).css({
        "transform-origin": "0 0",
        "-ms-transform-origin": "0 0",
        "-webkit-transform-origin": "0 0",
        "-moz-transform-origin": "0 0",
        "-o-transform-origin": "0 0"
    });
    function scaleStage() {
        var stage = $(ST2); // Set a reusable variable to reference the stage
        var parent = $(ST2).parent(); // Set a reusable variable to reference the parent container of the stage
        var parentWidth = stage.parent().width(); // Get the parent of the stage width
        var parentHeight = $(window).height(); // Get the browser window height
        var stageWidth = stage.width(); // Get the stage width
        var stageHeight = stage.height(); // Get the stage height
        var desiredWidth = Math.round(parentWidth * 1); // Set the new width of the stage as it scales
        var desiredHeight = Math.round(parentHeight * 1); // Set the new height of the stage as it scales
        var rescaleWidth = (desiredWidth / stageWidth); // Set a variable to calculate the new width of the stage as it scales
        var rescaleHeight = (desiredHeight / stageHeight); // Set a variable to calculate the new height of the stage as it scales
        var rescale = rescaleWidth;
        if (stageHeight * rescale > desiredHeight) // Do not scale larger than the height of the browser window
            rescale = rescaleHeight;
        stage.css("transform", "scale(" + rescale + ")");
        stage.css("-o-transform", "scale(" + rescale + ")");
        stage.css("-ms-transform", "scale(" + rescale + ")");
        stage.css("-webkit-transform", "scale(" + rescale + ")");
        stage.css("-moz-transform", "scale(" + rescale + ")");
        stage.css("-o-transform", "scale(" + rescale + ")");
        parent.height(stageHeight * rescale); // Reset the height of the parent container so the objects below it will reflow as the height adjusts
        var center = ((window.innerWidth - ($("#Stage").width() * rescale)) / 2);
        $("#Stage").css("left", center);
    }
    // Make it happen when the browser resizes
    $(window).on("resize", function () {
        scaleStage();
    });
    // Make it happen when the page first loads
    $(document).ready(function () {
        scaleStage();
    });
    main("ibero");
});